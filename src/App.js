import React, { useEffect } from "react";
import WebMap from "@arcgis/core/WebMap";
import MapView from "@arcgis/core/views/MapView";
import FeatureLayer from "@arcgis/core/layers/FeatureLayer";
import "./App.css";

export default function App() {
  useEffect(() => {
    const setupWebMap = () => {
      // Create a new Web map see -- https://developers.arcgis.com/javascript/latest/api-reference/esri-WebMap.html
      const webMap = new WebMap({
        basemap: "satellite",
      });

      // Create a new MapView see -- https://developers.arcgis.com/javascript/latest/api-reference/esri-views-MapView.html
      const mapView = new MapView({
        container: "mapView",
        map: webMap,
      });

      mapView.when((view) => {
        view.goTo({
          center: [175, -41],
        });
        view.zoom = 6;

        // Create a new Feature layer see -- https://developers.arcgis.com/javascript/latest/api-reference/esri-layers-FeatureLayer.html
        const waterQualityLayer = new FeatureLayer({
          // URL to online data that contains the water quality data
          url:
            "https://services1.arcgis.com/YAnRDYVL1tmpajaA/arcgis/rest/services/NZ_WaterQuality/FeatureServer/0",
        });

        webMap.add(waterQualityLayer);
      });
    };
    setupWebMap();
  }, []);

  return (
    <div className="app-container">
      {/* Load esri API and the map */}
      <div id="mapView" className="map-view" />
    </div>
  );
}
